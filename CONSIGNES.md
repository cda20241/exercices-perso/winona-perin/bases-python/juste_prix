**Le juste prix**

Vous devez créer un jeu du “Juste Prix” en Python. Dans ce jeu, l'ordinateur choisira un nombre aléatoire entre un minimum et un maximum définis. Ce nombre aléatoire est le juste prix. Le joueur devra deviner ce juste prix en faisant des propositions de nombres. A chaque proposition l'ordinateur lui répond « c’est plus » ou « c’est moins ». Il gagne lorsqu’il a trouvé le juste prix.

Le jeu doit fournir une interface en mode console. On aura trois menus dans notre interface :

    Menu de démarrage : affiche les 10 derniers résultats de joueurs, la possibilité d'aller au menu des résultats ou au menu de partie.
    Menu des résultats : affiche tous les résultats et permet à l'utilisateur de filtrer la liste par une recherche par nom de joueur. La liste doit s'actualiser à chaque changement de lettre saisie par l'utilisateur (ce n'est donc pas input qu'il faut utiliser, faire une recherche). Pour revenir au menu de démarrage l'utilisateur doit taper le mot “exit”.
    Menu de partie : Avant de démarrer une partie on demande à l'utilisateur de choisir un nom de joueur. Le nom “exit” ne peut pas être pris. Le nom ne doit pas inclure la chaîne de caractères “ : ”. On indique à l'utilisateur que la partie démarre dès qu'il a rentré son nom. Une fois la partie démarrée l'utilisateur voit uniquement sa réponse précédente et le résultat si “c'est plus” ou “c'est moins”. Lorsque le juste prix est trouvé la partie se termine et on affiche le score du joueur avec son nom, son chrono et son nombre de tentatives. Puis il doit appuyer sur “Entrée” pour revenir au menu de démarrage.

Un chrono doit être lancé pour compter le temps de la partie en secondes et millisecondes qui sera le score du joueur.

A chaque fin de partie il faut enregistrer la partie du joueur avec son nom et son score dans un fichier json et un fichier txt.

Le fichier txt doit être présenté sous la forme

NomDuJoueur1 : ScoreDuJoueur1\
NomDuJoueur2 : ScoreDuJoueur2\
etc

Pour afficher les résultats des joueurs utiliser au choix la sauvegarde avec le fichier json ou le fichier txt.

Commencez tout d'abord par lister au mieux les fonctions que vous allez devoir créer. Puis regroupez ces fonctions en différents packages.
Pour aller plus loin

Dans le package Resultats, dans une instruction if name == 'main': placer un exemple de code qui permet d'utiliser la fonction sauvegarde du package pour sauvegarder un résultat. Et un autre exemple de code qui permet d'utiliser la fonction recherche du package pour rechercher les résultats d'un joueur par son nom.

Ces exemples de code ne sont exécutés que lorsqu'on run le package.
